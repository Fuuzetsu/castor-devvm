This is a basic build infrastructure for spinning up a DevVM using packer, vagrant and puppet.

Tools
===
[Vagrant](http://www.vagrantup.com)
VirtualBox

If you use nix, `nix-shell` will give you a shell with what you need already.

Building the base image
===

Start by installing a [vagrant
plugin](https://github.com/dotless-de/vagrant-vbguest) that ensures
right guest additions are on guest VM:

```
vagrant plugin install vagrant-vbguest
```

Check out a CentOS 7 VBox image with puppet pre-installed on it:
```
vagrant init puppetlabs/centos-7.0-64-puppet
vagrant up --provider virtualbox
```

Ensure that the following files are under `puppet/files`:

```
lustre-client-2.7.0-3.10.0_229.7.2.el7.x86_64.x86_64.rpm
lustre-client-modules-2.7.0-3.10.0_229.7.2.el7.x86_64.x86_64.rpm
lustre-client-source-2.7.0-3.10.0_229.7.2.el7.x86_64.x86_64.rpm
```

Ask someone with integration cluster access to provide you these.

Bringing up a machine
===

```
vagrant up
```

The machine will install some stuff and likely pull in a more recent kernel. After it's done, run

```
vagrant reload
```

to reboot it.

Nearly done, just

```
vagrant ssh
```

and inside the machine in `/usr/src/lustre-2.7.0` run `sudo ./configue
&& sudo make`.

You should now be able to navigate to a mount of your mero repository and compile it. By default, hosts home directory is mounted at `/mnt/home` in the VM.
