{ stdenv, vagrant, packer }:

stdenv.mkDerivation {
  name = "castor-devvm";
  src = ./.;
  buildInputs = [ vagrant packer ];
}
