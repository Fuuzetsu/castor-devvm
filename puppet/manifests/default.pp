file { "/local":
  ensure => "directory",
  owner => "root",
  group => "root",
  mode => 755
}

file { "/local/home":
  ensure => "directory",
  owner => "root",
  group => "root",
  mode => 755
}

user { "devvm":
  home        => "/local/home/devvm",
  shell       => "/bin/bash",
  uid         => 1003,
  gid         => 502,
  managehome  => "true",
  password    => '$1$cSQp4LHk$oUGBjzScZyCEEC.3TPM3f/',
  groups      => ["devvm", "wheel", "users"],
  require     => File["/local/home"]
}

group { "devvm":
  gid => 502
}

yumrepo { "epel":
  mirrorlist => "http://mirrors.fedoraproject.org/mirrorlist?repo=epel-7&arch=x86_64",
  descr => "EPEL repository",
  enabled => 1,
  gpgcheck => 0
}

yumrepo { "petersen-ghc":
  baseurl => "http://copr-be.cloud.fedoraproject.org/results/petersen/ghc-7.10.1/epel-7-x86_64/",
  descr => "GHC Repository",
  enabled => 1,
  gpgcheck => 0
}

/*
yumrepo { "seagate-re":
  baseurl => "http://appdev-vm.xyus.xyratex.com/yum/devvm/OSAINT/latest",
  descr => "Seagate Release Engineering",
  enabled => 1,
  gpgcheck => 0
}
*/

$lustre_build_deps = [
  "libselinux-devel",
  "rpm-build"
]

$mero_build_deps = [
  "asciidoc",
  "autoconf",
  "automake",
  "binutils",
  "binutils-devel",
  "gcc",
  "gcc-c++",
  "gccxml",
  "genders",
  "glibc-headers",
  "kernel-devel",
  "libaio",
  "libaio-devel",
  "libtool",
  "libuuid-devel",
  "libyaml",
  "libyaml-devel",
  "make",
  "perl",
  "perl-File-Find-Rule",
  "perl-File-Slurp",
  "perl-IO-All",
  "perl-List-MoreUtils",
  "perl-XML-LibXML",
  "cpp",
  "docbook-dtds",
  "docbook-style-xsl",
  "glibc-devel",
  "libstdc++-devel",
  "m4",
  "mpfr",
  "perl-IO-String",
  "perl-Number-Compare",
  "perl-Spiffy",
  "perl-Text-Glob",
  "perl-XML-NamespaceSupport",
  "perl-XML-SAX",
  "redhat-lsb",
  "sgml-common",
  "systemd-devel",
  "xml-common",
  "git",
  "perl-Error",
  "perl-Git"
]

$mero_runtime_deps = [
  "perl-YAML-LibYAML",
  "perl-DateTime",
  "perl-File-Which",
  "expect",
  "perl-Class-Singleton",
  "perl-Params-Validate"
]

$halon_test_deps = [
  "docker",
  "telnet"
]

$halon_build_deps = [
  "ghc",
  "cabal-install",
  "leveldb-devel"
]


$sspl_runtime_deps = [
  "SSPL-LL",
  "rabbitmq-server",
  "python-sure",
  "python-fuzzywuzzy",
  "python-lettuce",
  "gemhpi",
  "lksctp-tools",
  "net-snmp-libs",
  "openhpi",
  "openhpi-libs",
  "python-daemon",
  "python-inotify",
  "python-jsonschema",
  "python-lockfile",
  "python-openhpi-baselib",
  "python-pika",
  "python-zope-interface",
  "python-zope-component",
  "zabbix-agent-lib",
  "zabbix-collector",
  "zabbix-openhpi-config"
]

package { $lustre_build_deps:
  ensure => "latest",
  require => Yumrepo["epel"]
}
package { $mero_build_deps:
  ensure => "latest",
  require => Yumrepo["epel"]
}
package { $mero_runtime_deps:
  ensure => "latest",
  require => Yumrepo["epel"]
}
package { $halon_build_deps:
  ensure => "latest",
  require => Yumrepo["petersen-ghc"]
}
package { $halon_test_deps:
  ensure => "latest",
  require => Yumrepo["epel"]
}
/*
package { $sspl_runtime_deps:
  ensure => "latest",
  require => Yumrepo["epel", "seagate-re"]
}
*/
/*
package { "lustre-client":
  ensure => "installed",
  provider => "yum",
  source => "/vagrant/puppet/files/lustre-client-2.7.0-3.10.0_229.7.2.el7.x86_64.x86_64.rpm",
  require => Package[$mero_build_deps]
}

package { "lustre-modules":
  ensure => "installed",
  provider => "yum",
  source => "/vagrant/puppet/files/lustre-client-modules-2.7.0-3.10.0_229.7.2.el7.x86_64.x86_64.rpm",
  require => Package[$mero_build_deps]
}

package { "lustre-source":
  ensure => "installed",
  provider => "yum",
  source => "/vagrant/puppet/files/lustre-client-source-2.7.0-3.10.0_229.7.2.el7.x86_64.x86_64.rpm",
  require => Package[$mero_build_deps],
  notify => Exec["prepare-lustre-sources"]
}
*/
exec { "install-lustre-packages":
  command => "yum install -y *.rpm || rpm --query lustre-client lustre-client-source lustre-client-modules",
  cwd => "/vagrant/puppet/files",
  logoutput => "true",
  /* notify => Exec["prepare-lustre-sources"], */
  path    => "/usr/local/bin/:/bin/",
  timeout => 0,
  require => Package[$mero_build_deps]
}
/*
exec { "prepare-lustre-sources":
  command => "pwd && ./configure && make",
  cwd => "/usr/src/lustre-2.7.0",
  logoutput => "true",
  path    => "/usr/local/bin/:/bin/",
  require => Exec["install-lustre-packages"],
  timeout => 0,
  subscribe => Exec["install-lustre-packages"]
}*/