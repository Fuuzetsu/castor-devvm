# Script to build and install the necessary lustre client libraries/modules

curl -O https://downloads.hpdd.intel.com/public/lustre/latest-feature-release/el7/client/SRPMS/lustre-client-2.7.0-3.10.0_123.20.1.el7.x86_64.src.rpm
# curl -O https://downloads.hpdd.intel.com/public/lustre/latest-feature-release/el7/client/SRPMS/lustre-dkms-2.7.0-1.el7.centos.src.rpm

rpmbuild --define '_topdir /rpmbuild' --rebuild --without servers lustre-client-2.7.0-3.10.0_123.20.1.el7.x86_64.src.rpm

cd /rpmbuild/RPMS/x86_64 && yum -y install *

# Need to make the lustre sources
cd /usr/src/lustre-2.7.0/ && ./configure && make
